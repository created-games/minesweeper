import Base from "base/index";

const routes = [{ path: "/base", alias: "/", load: Base }];

export default routes;
