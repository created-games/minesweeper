import React from "react";
import DOM from "react-dom";
import App from "./modules/index";
import config from "../config.json";
import "./assets/css/index.less";

DOM.render(<App />, document.getElementById(config.DOMRender.id));
