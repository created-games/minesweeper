export const randomInt = (min: number, max: number): number => {
  return Math.floor((max + 1) * Math.random() + min);
};

export const min = (numbers: number[]): number => {
  if (numbers.length == 0) return 0;
  if (numbers.length == 1) return numbers[0];
  let min = numbers[0];

  numbers.forEach((num) => {
    if (num < min) min = num;
  });

  return min;
};

export const max = (numbers: number[]): number => {
  if (numbers.length == 0) return 0;
  if (numbers.length == 1) return numbers[0];
  let max = numbers[0];

  numbers.forEach((num) => {
    if (num > max) max = num;
  });

  return max;
};
