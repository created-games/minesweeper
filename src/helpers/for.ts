type runForProps = {
  size: number;
  method: (i: number) => void;
};

export const runFor = (props: runForProps) => {
  const { size, method } = props;

  for (let i = 0; i < size; i++) method(i);
};
