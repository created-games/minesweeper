export const getElementSize = (ref: React.RefObject<any>) => {
  const current = ref?.current;

  return current
    ? { width: current.clientWidth, height: current.clientHeight }
    : { width: 0, height: 0 };
};
