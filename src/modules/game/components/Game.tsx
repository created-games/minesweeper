import React, { useEffect, useRef, useState } from "react";
import Square from "components/blocks/square";
import GameBoard from "./gameBoard";
import Configs from "../configs/map.json";
import { canPlay } from "../functions/Game";
import { gameStatusProps } from "../types";

const Game: React.FC = () => {
  const ref = useRef<HTMLDivElement>(null);
  const { boardSize } = Configs;
  const current = boardSize.easy;

  const [update, setUpdate] = useState(false);
  const [gameState, setGameState] = useState<gameStatusProps>("genmap");

  useEffect(() => {
    !update && setUpdate(true);
  }, [update]);

  return (
    <div ref={ref} className="h100p w100p minesweeper-wrapper">
      <div className="w-fit minesweeper">
        <Square
          Header={<div onClick={() => setGameState("restart")}>Reset</div>}
          Content={
            <GameBoard
              size={{ x: current.x, y: current.y }}
              mines={current.mines}
              canPlay={canPlay(gameState)}
              gameState={gameState}
              setGameState={(state: gameStatusProps) => setGameState(state)}
            />
          }
          update={update}
          reference={ref}
        />
      </div>
    </div>
  );
};

export default Game;
