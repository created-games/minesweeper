import React, { useEffect, useState } from "react";
import Configs from "../configs/map.json";
import { gameStatusProps } from "../types";
import { genMap, openField } from "../functions/gameBoard";
import GameField from "./gameField";
import { num2D } from "types/index";

type GameBoard = {
  size: num2D;
  mines: number;
  canPlay: boolean;
  gameState: gameStatusProps;
  setGameState: (state: gameStatusProps) => void;
};

const GameBoard: React.FC<GameBoard> = (props) => {
  const { size, mines, canPlay, gameState, setGameState } = props;
  const { min, max } = Configs.field.size;

  const [map, setMap] = useState<any[][]>([]);
  const [leftFields, setLeftFields] = useState<number>(0);

  useEffect(() => {
    if (gameState == "genmap" || gameState == "restart") {
      const { data, left } = genMap(size, mines);
      setMap(data);
      setLeftFields(left);
      setGameState("ready");
    }
  }, [gameState]);

  const onLeftClick = (x: number, y: number) => {
    if (map[x][y].content == -1) setGameState("lost");
    const trail = openField({ x, y }, [...map], leftFields, size);
    setMap(trail.data);
    leftFields !== trail.left && setLeftFields(trail.left);
  };

  const content = () =>
    map.map((row, x) =>
      row.map((field, y) => {
        const cords = { x, y };
        return (
          <GameField
            key={`${x}-${y}`}
            canPlay={canPlay}
            onLeftClick={onLeftClick}
            onRightClick={(x, y) => console.log("right click")}
            {...cords}
            {...field}
          />
        );
      })
    );

  return (
    <div
      className="w100p h100p gameBoard dGrid"
      style={{
        gridTemplateRows: `repeat(${size.x}, minmax(${min}px, ${max}px))`,
        gridTemplateColumns: `repeat(${size.y}, minmax(${min}px, ${max}px))`,
      }}
    >
      {content()}
    </div>
  );
};

export default GameBoard;
