import React from "react";
import classNames from "classnames";

type fieldProps = {
  opened: boolean;
  content: number;
  x: number;
  y: number;
};

type gameFieldProps = fieldProps & {
  canPlay: boolean;
  onLeftClick: (x: number, y: number) => void;
  onRightClick?: (x: number, y: number) => void;
};

const GameField: React.FC<gameFieldProps> = (props) => {
  const { x, y, content, opened, onLeftClick, onRightClick, canPlay } = props;

  return (
    <div
      className={classNames("field", { opened: opened })}
      onClick={() => canPlay && onLeftClick(x, y)}
      onContextMenu={() => canPlay && onRightClick && onRightClick(x, y)}
    >
      {opened && content != 0 && content}
    </div>
  );
};

export default GameField;
export type { fieldProps };
