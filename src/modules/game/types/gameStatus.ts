import { gameStatusProps as base } from "types/gameStatus";

export type gameStatusProps = base | "genmap";
