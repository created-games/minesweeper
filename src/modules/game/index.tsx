import React from "react";
import Layout from "layout/C";
import Game from "./components/Game";

const Index: React.FC = () => {
  return <Layout name="game" content={<Game />} />;
};

export default Index;
