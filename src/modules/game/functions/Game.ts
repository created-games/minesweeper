import { gameStatusProps } from "../types";

export const canPlay = (gameStatus: gameStatusProps): boolean => {
  const allowed: gameStatusProps[] = ["ready", "started", "inprogress"];

  return allowed.includes(gameStatus);
};
