import { runFor } from "helpers/for";
import { randomInt } from "helpers/math";
import { num2D } from "types/index";

const genBomb = ({ x, y }: num2D) => {
  return { x: randomInt(0, x - 1), y: randomInt(0, y - 1) };
};

const addBombsToMap = (map: any[][], mines: number, size: num2D) => {
  let arr = [...map];
  let mineCount = 0;

  while (mineCount < mines) {
    const { x, y } = genBomb(size);

    -1 != arr[x][y].content &&
      ((arr[x][y] = { ...arr[x][y], content: -1 }), mineCount++),
      0 <= y - 1 &&
        -1 != arr[x][y - 1].content &&
        (arr[x][y - 1] = {
          ...arr[x][y - 1],
          content: arr[x][y - 1].content + 1,
        }),
      y + 1 < size.y &&
        -1 != arr[x][y + 1].content &&
        (arr[x][y + 1] = {
          ...arr[x][y + 1],
          content: arr[x][y + 1].content + 1,
        }),
      0 <= x - 1 &&
        -1 != arr[x - 1][y].content &&
        (arr[x - 1][y] = {
          ...arr[x - 1][y],
          content: arr[x - 1][y].content + 1,
        }),
      x + 1 < size.x &&
        -1 != arr[x + 1][y].content &&
        (arr[x + 1][y] = {
          ...arr[x + 1][y],
          content: arr[x + 1][y].content + 1,
        }),
      0 <= y - 1 &&
        0 <= x - 1 &&
        -1 != arr[x - 1][y - 1].content &&
        (arr[x - 1][y - 1] = {
          ...arr[x - 1][y - 1],
          content: arr[x - 1][y - 1].content + 1,
        }),
      0 <= y - 1 &&
        x + 1 < size.x &&
        -1 != arr[x + 1][y - 1].content &&
        (arr[x + 1][y - 1] = {
          ...arr[x + 1][y - 1],
          content: arr[x + 1][y - 1].content + 1,
        }),
      y + 1 < size.y &&
        0 <= x - 1 &&
        -1 != arr[x - 1][y + 1].content &&
        (arr[x - 1][y + 1] = {
          ...arr[x - 1][y + 1],
          content: arr[x - 1][y + 1].content + 1,
        }),
      y + 1 < size.y &&
        x + 1 < size.x &&
        -1 != arr[x + 1][y + 1].content &&
        (arr[x + 1][y + 1] = {
          ...arr[x + 1][y + 1],
          content: arr[x + 1][y + 1].content + 1,
        });
  }

  return arr;
};

const genEmptyMap = ({ x, y }: num2D) => {
  let arr: any[][] = [];

  runFor({
    size: x,
    method: (i: number) => {
      arr[i] = [];
      runFor({
        size: y,
        method: (j: number) => {
          arr[i][j] = { opened: false, content: 0 };
        },
      });
    },
  });

  return arr;
};

export const openField = (
  { x, y }: num2D,
  map: any[][],
  leftField: number,
  size: num2D
): { data: any[][]; left: number } => {
  if (map[x][y].opened) return { data: map, left: leftField };
  if (map[x][y].content !== -1) leftField -= 1;

  let opened: { data: any[][]; left: number } = { data: map, left: leftField };
  const { data, left } = opened;

  data[x][y] = { ...data[x][y], opened: true };

  if (0 == data[x][y].content) {
    if (0 <= x - 1 && !data[x - 1][y].opened)
      opened = openField({ x: x - 1, y }, data, left, size);

    if (x + 1 < size.x && !data[x + 1][y].opened)
      opened = openField({ x: x + 1, y }, data, left, size);

    if (0 <= y - 1 && !data[x][y - 1].opened)
      opened = openField({ x, y: y - 1 }, data, left, size);

    if (y + 1 < size.y && !data[x][y + 1].opened)
      opened = openField({ x, y: y + 1 }, data, left, size);
  }

  return opened;
};

export const genMap = (size: num2D, mines: number) => {
  let arr: any[][] = genEmptyMap(size);
  arr = addBombsToMap(arr, mines, size);
  return { data: arr, left: size.x * size.y };
};
