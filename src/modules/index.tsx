import React, { useState, useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import CoreIndex from "core/index";

const Index: React.FC = () => {
  const [size, setSize] = useState({
    x: window.innerWidth,
    y: window.innerHeight,
  });

  useEffect(() => (window.onresize = updateSize), []);

  const updateSize = () =>
    setSize({
      x: window.innerWidth,
      y: window.innerHeight,
    });

  return (
    <BrowserRouter>
      <CoreIndex />
    </BrowserRouter>
  );
};

export default Index;
