export type gameStatusProps =
  | "ready"
  | "started"
  | "inprogress"
  | "restart"
  | "lost"
  | "won";
