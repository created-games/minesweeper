import React from "react";
import { Route, Switch } from "react-router-dom";
import Base from "base/index";
import Game from "game/index";

const App: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Game />
      </Route>
    </Switch>
  );
};

export default App;
