import React from "react";
import TaskBar from "components/layout/taskbar";
import { Layout as LayoutD } from "antd";
import classNames from "classnames";

type baseProps = {
  name: string;
  children: React.ReactNode;
};

const Base: React.FC<baseProps> = ({ children, name }) => {
  return (
    <LayoutD className={classNames("h100p", name)}>
      <TaskBar />
      {children}
    </LayoutD>
  );
};

export default Base;
export type { baseProps };
