import React from "react";
import Base, { baseProps } from "layout/base";
import Content from "components/layout/content";
import classNames from "classnames";

type cProps = Omit<baseProps, "children"> & {
  content: React.ReactElement;
};

const C: React.FC<cProps> = (props) => {
  const { content, ...restProps } = props;

  return (
    <Base {...restProps}>
      <Content className={classNames("h100p")}>{content}</Content>
    </Base>
  );
};

export default C;
