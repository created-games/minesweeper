import React from "react";
import classNames from "classnames";
import MacBtn from "./macButton";

import { Btn, btnMacProps, btnWindowProps, isWindows } from "./btn";

type closeBtnProps = (btnMacProps | btnWindowProps) & {
  className?: string;
};

const CloseBtn: React.FC<closeBtnProps> = (props) => {
  const { className } = props;

  let renderprops = {
    className: classNames(className),
  };

  // style === windows
  if (isWindows(props)) {
    const { title } = props;

    return <Btn {...renderprops}>{title}</Btn>;
  }

  // style === mac (default)
  return (
    <MacBtn color="red" {...renderprops}>
      <span />
    </MacBtn>
  );
};

export default CloseBtn;
