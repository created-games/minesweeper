import React from "react";
import classNames from "classnames";
import { Btn } from "./btn";

type closeBtnProps = {
  color: "red" | "orange" | "green";
  className?: string;
};

const CloseBtn: React.FC<closeBtnProps> = (props) => {
  const { className, color } = props;

  return (
    <Btn className={classNames(className, color, "mac")}>
      <span />
    </Btn>
  );
};

export default CloseBtn;
