import { Button as Btn, ButtonProps as BtnProps } from "antd";

type btnMacProps = {
  style?: "mac";
};

type btnWindowProps = {
  style?: "windows";
  title: string | number | React.ReactNode;
};

const isMac = (x: any): x is btnMacProps => x.style == "mac";
const isWindows = (x: any): x is btnWindowProps => x.style == "windows";

export type { BtnProps, btnMacProps, btnWindowProps };
export { isMac, isWindows };
export { Btn };
