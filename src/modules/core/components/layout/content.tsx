import React from "react";
import { Layout } from "antd";

type contentProps = {
  children: React.ReactNode;
  className?: string;
};

const Content: React.FC<contentProps> = ({ children, className }) => {
  return <Layout.Content className={className}>{children}</Layout.Content>;
};

export default Content;
