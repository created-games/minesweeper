import React from "react";
import { Row, Col } from "antd";
import { CloseBtn } from "components/buttons/index";
import MacBtn from "components/buttons/macButton";

const TaskBar: React.FC = () => {
  return (
    <Row wrap={false} id="taskbar">
      <Col flex="none">
        <div>Minesweeper</div>
      </Col>
      <Col flex="auto" className="txt-c">
        Classic
      </Col>
      <Col flex="none">
        <div className="actionButtons">
          <div className="action options">*</div>
          <div className="action">
            <MacBtn className="lower" color="green" />
          </div>
          <div className="action minmax">
            <MacBtn className="minmax" color="orange" />
          </div>
          <div className="action">
            <CloseBtn className="close" style="mac" />
          </div>
        </div>
      </Col>
    </Row>
  );
};

export default TaskBar;
