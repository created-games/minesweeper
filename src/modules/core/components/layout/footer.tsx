import React from "react";
import { Layout } from "antd";

const Footer: React.FC = () => {
  return <Layout.Footer>Footer</Layout.Footer>;
};

export default Footer;
