import React, { useEffect, useRef, useState } from "react";
import { min, max } from "helpers/math";
import { getElementSize } from "helpers/size";

type square = {
  Header?: React.ReactNode;
  Content: React.ReactNode;
  Footer?: React.ReactNode;
  update: boolean;
  reference: React.RefObject<HTMLDivElement>;
  sizing?: "min" | "max";
};

const Square: React.FC<square> = (props) => {
  const { Header, Content, Footer, reference, sizing = "min" } = props;
  const headerRef = useRef<HTMLDivElement>(null);
  const footerRef = useRef<HTMLDivElement>(null);

  const [size, setSize] = useState(0);

  useEffect(() => {
    const current = reference?.current;

    if (current) {
      const func = sizing == "min" ? min : max;
      const headerSize = getElementSize(headerRef);
      const contentSize = getElementSize(reference);
      const footerSize = getElementSize(footerRef);

      const leftHeight =
        contentSize.height - headerSize.height - footerSize.height;

      setSize(func([contentSize.width, leftHeight]));
    }
  });

  return (
    <>
      {Header && (
        <div ref={headerRef} style={{ width: `${size}px` }}>
          {Header}
        </div>
      )}
      <div style={{ width: `${size}px`, height: `${size}px` }}>{Content}</div>
      {Footer && (
        <div ref={footerRef} style={{ width: `${size}px` }}>
          {Footer}
        </div>
      )}
    </>
  );
};

export default Square;
