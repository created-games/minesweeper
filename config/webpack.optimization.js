const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const prod = {
  minimizer: [
    new CssMinimizerPlugin(),
    new TerserPlugin({
      terserOptions: {
        output: {
          comments: false,
        },
      },
    }),
  ],
  minimize: true,
};

const dev = {};

module.exports = function optimization(idDev) {
  return idDev ? dev : prod;
};
